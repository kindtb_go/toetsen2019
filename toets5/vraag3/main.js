function getData() {
    var data = [];
    for (var i=0; i<10; i++) {
        var object = {
            'nummer': i+1,
            'aantal_ogen': Math.floor(1+3*Math.random()),
            'aantal_poten': Math.floor(8*Math.random()),
            'kan_vliegen': Math.random() < 0.5
        };
        data.push(object);
    }
    return data;
}