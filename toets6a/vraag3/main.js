function getData() {
    var data = [];
    var merken = ["BMW", "Audi", "Opel", "Mercedes", "Ford", "Renault"];
    var aantalMerken = merken.length;
    var kleuren = ["Rood", "Wit", "Zwart", "Geel"];
    var aantalKleuren = kleuren.length;

    var karakters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var aantalKarakters = karakters.length;
    var cijfers = "01234567890";
    var aantalCijfers = cijfers.length;


    var aantalAutos = Math.floor(500+500*Math.random());
    for (var i=0; i<aantalAutos; i++) {
        var nummerPlaat = '';
        nummerPlaat+= karakters[Math.floor(aantalKarakters*Math.random())];
        nummerPlaat+= karakters[Math.floor(aantalKarakters*Math.random())];
        nummerPlaat+= karakters[Math.floor(aantalKarakters*Math.random())];
        nummerPlaat+= cijfers[Math.floor(aantalCijfers*Math.random())];
        nummerPlaat+= cijfers[Math.floor(aantalCijfers*Math.random())];
        nummerPlaat+= cijfers[Math.floor(aantalCijfers*Math.random())];
        var object = {
            'nummer': i+1,
            'merk': merken[Math.floor(aantalMerken*Math.random())],
            'kleur': kleuren[Math.floor(aantalKleuren*Math.random())],
            'verzekerd': Math.random() < 0.5,
            'nummerplaat' : nummerPlaat
        };
        data.push(object);
    }
    return data;
}