function getData(size) {
    var data = [];
    for (var i=0; i<size; i++) {
        var rij = [];
        for (var j=0; j<size; j++) {
            var color = '#';
            for (var k=0; k<3; k++) {
                var part = Math.floor(256*Math.random()).toString(16);
                if (part.length<2) part = '0'+part;
                color+= part;
            }
            rij.push(color);
        }
        data.push(rij);
    }
    return data;
}